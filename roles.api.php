<?php

/**
 * @file
 * Provide API to using by other modules.
 */

/**
 * Register your own group of role.
 */
function hook_roles_group() {
  // We create a group named group_admin has three roles as member
  // they are administrator, maintenancer, supervision.
  // The name of role must the machine name, not label.
  return array(
    'group_admin' => array(
      'administrator',
      'maintenancer',
      'supervision',
    ),
  );
}
